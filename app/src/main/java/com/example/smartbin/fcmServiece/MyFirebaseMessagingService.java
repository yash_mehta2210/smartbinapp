package com.example.smartbin.fcmServiece;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.os.Build;
import android.text.TextUtils;
import android.util.Log;

import com.example.smartbin.R;
import com.example.smartbin.retrofit.ServiceURL;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.pixplicity.easyprefs.library.Prefs;

import java.util.HashMap;
import java.util.Map;

import androidx.core.app.NotificationCompat;


public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static int NOTIFY_ID = 0;

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        Map<String, String> dataMap = new HashMap<>();
        if (remoteMessage.getData() != null) {
            dataMap = remoteMessage.getData();

            for (String key : dataMap.keySet())
                Log.e("FIREBASE DATA", "FIRE " + key + " = " + dataMap.get(key));
        }
/*
        if (remoteMessage.getNotification() != null) {
            Log.e("FIREBASE", "FIRE1 " + remoteMessage.getNotification().getTitle());
            Log.e("FIREBASE", "FIRE1 " + remoteMessage.getNotification().getBody());
            Log.e("FIREBASE", "FIRE1 " + remoteMessage.getNotification().getTag());
        }*/

//        if (SharedPreferenceUtil.getUserModel() == null)
//            return;

        createNotification(/*remoteMessage.getNotification().getTitle(), remoteMessage.getNotification().getBody(),*/dataMap);

    }

    public void createNotification(/*String sendername, String msg,*/Map<String, String> dataMap) {
        String channelId = getString(R.string.notification_channel_id_message);
        String title = getString(R.string.app_name); // Default Channel
        String sendername = dataMap.get("title");
        String msg = dataMap.get("body");

        NotificationCompat.Builder builder;
        NotificationManager notifManager = null;
        if (notifManager == null) {
            notifManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel mChannel = notifManager.getNotificationChannel(channelId);
            if (mChannel == null) {
                mChannel = new NotificationChannel(channelId, title, importance);
                mChannel.enableVibration(true);
                mChannel.setVibrationPattern(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});
                notifManager.createNotificationChannel(mChannel);
            }

        }
        builder = new NotificationCompat.Builder(this, channelId);
        String notificationType = "";


        builder.setContentTitle(sendername)                            // required
                .setSmallIcon(R.drawable.notification_logo)
//                .setSmallIcon((Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) ? R.drawable.ic_trans_notification : R.mipmap.ic_launcher)// required
                .setColor(getResources().getColor(R.color.colorPrimary))
                .setContentText(msg) // required
                .setDefaults(Notification.DEFAULT_ALL)
                .setAutoCancel(true)
                .setVibrate(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400})
                .setPriority(Notification.PRIORITY_HIGH);

        Notification notification = builder.build();
        notifManager.notify(NOTIFY_ID, notification);
        NOTIFY_ID++;
    }

//        private void sendNotification(String senderName,String message){
//        NotificationCompat.Builder builder = new NotificationCompat.Builder(getApplicationContext(),
//                getString(R.string.notification_channel_id_message));
//        builder.setContentTitle(senderName);
//        builder.setContentText(Html.fromHtml(message));
//        builder.setStyle(new NotificationCompat.BigTextStyle().bigText(Html.fromHtml(message)));
//        builder.setSmallIcon(R.mipmap.ic_launcher);
//        builder.setLargeIcon(BitmapFactory.decodeResource(this.getResources(), R.mipmap.ic_launcher));
//        builder.setPriority(Notification.PRIORITY_HIGH);
//        builder.setDefaults(NotificationCompat.DEFAULT_ALL);
//        builder.setAutoCancel(true);
//
//        Intent notificationIntent = new Intent(this, MessageListActivity.class);
//        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//
//        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, notificationIntent, 0);
//
//            builder.setContentIntent(pendingIntent);
//
//        Notification notification = builder.build();
//        notifManager.notify(NOTIFY_ID, notification);
//    }
    @Override
    public void onNewToken(String refreshedToken) {
        super.onNewToken(refreshedToken);
        if (!TextUtils.isEmpty(refreshedToken)) {
            Log.e("TOKEN>>", refreshedToken);
            Prefs.putString(ServiceURL.fcmToken, refreshedToken);
//            SharedPreferenceUtil.setFCMToken(refreshedToken);
        }
    }


}